Molecule
========

Dependencies
------------

Install and configure Libvirt and a container engine (Docker or Podman).

Test'n dev
----------

### Using Docker

```sh
docker build --pull --iidfile /tmp/iid .
```

Test Docker in Docker :

```sh
docker run --rm -t --privileged --security-opt label=disable \
    -e MOLECULE_CONTAINERS_BACKEND=docker \
    -v /run/docker.sock:/run/docker.sock \
    -v $PWD:/work -w /work $(cat /tmp/iid) \
    molecule test
```

Test Podman in Docker :

```sh
docker run --rm -t --privileged --security-opt label=disable \
    -e MOLECULE_CONTAINERS_BACKEND=podman \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v $PWD:/work -w /work $(cat /tmp/iid) \
    molecule test
```

Test Vagrant in Docker :

```sh
docker run --rm -t --privileged --security-opt label=disable \
    -v /run/libvirt:/run/libvirt \
    -v $HOME/.vagrant.d:/root/.vagrant.d \
    -v $PWD:/work -w /work $(cat /tmp/iid) \
    molecule test -s vagrant
```

Debug inside the container :

```sh
docker run --rm -ti --privileged --security-opt label=disable \
    -v /run/docker.sock:/run/docker.sock \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v /run/libvirt:/run/libvirt \
    -v $HOME/.vagrant.d:/root/.vagrant.d \
    -v $PWD:/work -w /work $(cat /tmp/iid) \
    sh
# Test Vagrant
vagrant up
vagrant destroy
# Test Docker
docker run --rm hello-world
# Test Podman
podman run --rm hello-world
# Test molecule
MOLECULE_CONTAINERS_BACKEND=docker molecule test
MOLECULE_CONTAINERS_BACKEND=podman molecule test
molecule test -s vagrant
```

### Using Podman

```sh
systemctl start --user podman
podman build --pull=newer --iidfile /tmp/iid .
```

Test Docker in Podman :

```sh
podman run --rm -t --privileged --security-opt label=disable \
    -e MOLECULE_CONTAINERS_BACKEND=docker \
    -v $XDG_RUNTIME_DIR/podman/podman.sock:/run/docker.sock \
    -v $PWD:/work -w /work $(cat /tmp/iid) \
    molecule test
```

Test Podman in Podman :

```sh
podman run --rm -t --privileged --security-opt label=disable \
    -e MOLECULE_CONTAINERS_BACKEND=podman \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v $PWD:/work -w /work $(cat /tmp/iid) \
    molecule test
```

Test Vagrant in Podman :

```sh
podman run --rm -t --privileged --security-opt label=disable \
    -v /run/libvirt:/run/libvirt \
    -v $HOME/.vagrant.d:/root/.vagrant.d \
    -v $PWD:/work -w /work $(cat /tmp/iid) \
    molecule test -s vagrant
```

Debug inside the container :

```sh
podman run --rm -ti --privileged --security-opt label=disable \
    -v $XDG_RUNTIME_DIR/podman/podman.sock:/run/docker.sock \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v /run/libvirt:/run/libvirt \
    -v /var/lib/libvirt:/var/lib/libvirt \
    -v $HOME/.vagrant.d:/root/.vagrant.d \
    -v $PWD:/work -w /work $(cat /tmp/iid)
# Test Vagrant
vagrant up
vagrant destroy
# Test Docker
docker run --rm hello-world
# Test Podman
podman run --rm hello-world
# Test molecule
MOLECULE_CONTAINERS_BACKEND=docker molecule test
MOLECULE_CONTAINERS_BACKEND=podman molecule test
molecule test -s vagrant
```
