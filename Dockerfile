FROM gwerlas/ansible:debian

# Dependencies
RUN apt-get update && \
    apt-get install -y \
        ansible-lint \
        kmod qemu-utils vagrant-libvirt python3-vagrant \
        python3-click-help-colors python3-commonmark && \
    rm -rf /var/lib/apt/lists/*

# Molecule
RUN pip install \
        --break-system-packages \
        --root-user-action ignore \
        --no-cache-dir \
        --exists-action i \
        molecule molecule-plugins

# Fix ERROR! couldn't resolve module/action 'vagrant'
ENV ANSIBLE_LIBRARY=/usr/local/lib/python3.13/dist-packages/molecule_plugins/vagrant/modules/

ENV USER=root
