Molecule
========

[![pipeline status](https://gitlab.com/yoanncolin/container-images/molecule/badges/main/pipeline.svg)](https://gitlab.com/yoanncolin/container-images/molecule/-/commits/main)

[Molecule] container image with [Molecule plugins][].

GitLab Project : [yoanncolin/container-images/molecule][].

[yoanncolin/container-images/molecule]: https://gitlab.com/yoanncolin/container-images/molecule
[Molecule]: https://github.com/ansible-community/molecule
[Molecule plugins]: https://github.com/ansible-community/molecule-plugins

Tags
----

* [`25.2.0`][latest], [`25.2`][latest], [`25`][latest], [`latest`][latest]
* [`24.12.0`][24.12.0], [`24.12`][24.12.0], [`24`][24.12.0]
* [`24.2.1`][24.12.0], [`24.2`][24.12.0]
* [`24.2.0`][24.12.0]
* [`6.0.3`][6.0.3], [`6.0`][6.0.3], [`6`][6.0.3]
* [`6.0.2`][6.0.3]
* [`6.0.1`][6.0.3]
* [`6.0.0`][6.0.3]
* [`5.1.0`][5.1.0], [`5.1`][5.1.0], [`5`][5.1.0]
* [`5.0.1`][5.0.1], [`5.0`][5.0.1]
* [`5.0.0`][5.0.1]
* [`4.0.4`][5.0.1], [`4.0`][5.0.1], [`4`][5.0.1]

[latest]: https://gitlab.com/yoanncolin/container-images/molecule/-/blob/main/Dockerfile
[24.12.0]: https://gitlab.com/yoanncolin/container-images/molecule/-/blob/24.12.0/Dockerfile
[6.0.3]: https://gitlab.com/yoanncolin/container-images/molecule/-/blob/6.0.3/Dockerfile
[5.1.0]: https://gitlab.com/yoanncolin/container-images/molecule/-/blob/5.1.0/Dockerfile
[5.0.1]: https://gitlab.com/yoanncolin/container-images/molecule/-/blob/5.0.1/Dockerfile

Requirements
------------

You'll need to have a configured and running libvirt for virtual machines based
tests scenarios.

You'll need to have a configured container engine (Docker or Podman) for
containers based tests scenarios.

Usage
-----

```sh
docker run -t --rm \
  --privileged \
  -v /run/libvirt:/run/libvirt \
  -v /var/lib/libvirt:/var/lib/libvirt \
  -v $HOME/.vagrant.d:/root/.vagrant.d \
  -v /run/docker.sock:/run/docker.sock \
  -v $HOME/.ansible:/root/.ansible \
  -v $HOME/.cache/molecule:/root/.cache/molecule \
  -v $PWD:$PWD \
  -w $PWD \
  gwerlas/molecule \
  molecule test
```

License
-------

[BSD 3-Clause License](LICENSE).
